from django.db import models

# Create your models here.
class MyUser(models.Model):
    username = models.CharField(max_length=32)
    password = models.CharField(max_length=256)
    email = models.CharField(max_length=40)


class Gift(models.Model):
    name = models.CharField(max_length=32)
    user = models.ForeignKey(MyUser, on_delete=models.CASCADE)
