from django.shortcuts import render, redirect

# Create your views here.
from django.views import View
from wishlist.models import MyUser, Gift


class Signin(View):
    def get(self, request):
        if request.session.get("username"):
            return redirect("user")
        return render(request, "signin.html")

    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']
        user = MyUser.objects.all().filter(username=username)

        if user.count() == 0 or user[0].password != password:
            return render(request, "signin.html", {"message": "username/password incorrect"})

        request.session["username"] = username
        return redirect("user")


class Signup(View):
    def get(self, request):
        return render(request, 'signup.html')

    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']

        if MyUser.objects.all().filter(username=username).count() != 0:
            return render(request, "signup.html", {"message": f'{username} already exists'})

        MyUser(username=username, password=password, email=email).save()

        return redirect('signin')


class Gifts(View):
    def get(self, request, **kwargs):
        if not request.session.get("username"):
            return redirect("signin")

        username = self.kwargs["username"] if "username" in self.kwargs else request.session["username"]
        gifts = Gift.objects.filter(user__username=username).all()

        return render(request, "gifts.html", {"gifts": gifts, "username": username})


class User(View):
    def get(self, request):
        if not request.session.get("username"):
            return redirect("signin")

        username = request.session["username"]
        users = MyUser.objects.all()

        return render(request, "user.html", {"username": username, "users": users})

    def post(self, request):
        gift_name = request.POST["gift_name"]
        username = request.session["username"]
        user = MyUser.objects.get(username=username)
        Gift(name=gift_name, user=user).save()

        return redirect("user")


class Logout(View):
    def get(self, request):
        request.session.pop("username", None)
        return redirect("signin")
